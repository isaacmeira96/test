# React + TypeScript + Vite

How to run:

Make sure you have a node version >= 16

First you need install all dependencies doing `yarn`.

After installation you can simply run `yarn run dev`
The app will run on `http://localhost:5173/` 

How to test: 

If you want test to see all itens covered by coverage, do
`yarn run test --coverage`

All thoughts about, architectura decisions and so on will be discussed. 