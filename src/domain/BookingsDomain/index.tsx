import { FC, useContext, useState } from "react";
import { Card } from "../../components/Card";
import { BookingContext } from "../../contexts/BookingContext";
import { EmptyState } from "../../components/EmptyState";
import { BookingProps } from "../../contexts/BookingContext/types";
import { isEmpty } from "lodash";

import "./styles.scss";
import { DeletionModal } from "../../components/Modals/DeletionModal";

export const BookingsDomain: FC = () => {
  const { bookings, deleteBooking, updateBooking } = useContext(BookingContext);
  const [editModal, setEditModal] = useState(false);

  const handleDeleteBook = (id: string) => {
    DeletionModal({
      deletedText: "The booking has been deleted",
      deleteAction: () => deleteBooking(id),
    });
  };

  const handleBookingUpdate = (id: string, updatedDetails: BookingProps) => {
    updateBooking(id, updatedDetails);
  };

  return (
    <div className="bookings-domain">
      {!isEmpty(bookings) && (
        <span className="bookings-domain__title">Manage your bookings</span>
      )}

      <div className="bookings-domain__cards">
        {!isEmpty(bookings) ? (
          bookings.map(
            ({ startDate, endDate, location, price, guests, id }) => (
              <Card
                onDelete={() => handleDeleteBook(id)}
                onClick={() => setEditModal(true)}
                key={id}
                {...{ startDate, endDate, location, price, guests, id }}
              >
                Edit Booking
              </Card>
            )
          )
        ) : (
          <EmptyState
            title="You don't have any bookings yet"
            description="Once you have bookings you will see they here"
          />
        )}
      </div>
    </div>
  );
};
