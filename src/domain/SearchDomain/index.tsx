import { FC, useContext } from "react";
import { Card } from "../../components/Card";

import { API_RETURN } from "../../mocks/searchReturn";
import { BookingProps } from "../../contexts/BookingContext/types";
import "./styles.scss";
import { BookingContext } from "../../contexts/BookingContext";

export const SearchDomain: FC = () => {
  /** Here i would call an api
   *  service do get places, this is why i created this domain
   *  Also, i will use the context to store the booking data
   */

  const { createBooking } = useContext(BookingContext);

  const handleAddBook = (booking: BookingProps) => {
    console.log(booking);
    createBooking(booking);
  };

  const cards = API_RETURN as BookingProps[];

  return (
    <div className="search-domain">
      <span className="search-domain__title">
        Here is our available accommodations
      </span>
      <div className="search-domain__cards">
        {cards.map(({ startDate, endDate, location, price, guests, id }) => (
          <Card
            onClick={() =>
              handleAddBook({ startDate, endDate, location, price, guests, id })
            }
            key={id}
            {...{ startDate, endDate, location, price, guests, id }}
          >
            Book
          </Card>
        ))}
      </div>
    </div>
  );
};
