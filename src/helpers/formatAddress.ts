import { LocationProps } from "../contexts/BookingContext/types";

export const formatAddress = (location: LocationProps) => {
  const { address, city, state, country } = location;
  return `${address} - ${city} - ${state} - ${country}`;
};
