import { formatAddress } from "../formatAddress";
import { LocationProps } from "../../contexts/BookingContext/types";

describe("formatAddress", () => {
  it("should formats the address correctly", () => {
    const location = {
      address: "123 Main St",
      city: "Example City",
      state: "CA",
      country: "US",
    } as LocationProps;

    const formattedAddress = formatAddress(location);

    expect(formattedAddress).toBe("123 Main St - Example City - CA - US");
  });
});
