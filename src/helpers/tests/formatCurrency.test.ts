import { formatCurrency } from "../formatCurrency";

describe("formatCurrency", () => {
  it("should formats currency correctly", () => {
    const price = 1000;

    const formattedPrice = formatCurrency(price);

    expect(formattedPrice).toBe("$1,000.00");
  });

  it("should handles negative numbers correctly", () => {
    const price = -500;

    const formattedPrice = formatCurrency(price);

    expect(formattedPrice).toBe("-$500.00");
  });

  it("should handles zero correctly", () => {
    const price = 0;

    const formattedPrice = formatCurrency(price);

    expect(formattedPrice).toBe("$0.00");
  });
});
