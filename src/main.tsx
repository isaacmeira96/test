import React from "react";
import ReactDOM from "react-dom/client";
import Main from "./pages/Home/Home";
import "./styles/global/reset.scss";
import "./styles/global/root.scss";

import "react-date-range/dist/styles.css"; // main style file
import "react-date-range/dist/theme/default.css"; // theme css file
import { BookingProvider } from "./contexts/BookingContext";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <BookingProvider>
      <Main />
    </BookingProvider>
  </React.StrictMode>
);
