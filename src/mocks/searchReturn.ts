export const API_RETURN = [
  {
    id: "1",
    location: {
      city: "New York",
      state: "NY",
      country: "US",
      address: "123 Main St",
      name: "Hotel 1",
      image:
        "https://images.pexels.com/photos/164595/pexels-photo-164595.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
    },
    startDate: "2024-01-01",
    endDate: "2024-01-31",
    price: 1000,
    guests: {
      adults: 2,
      children: 0,
      rooms: 1,
    },
  },
  {
    id: "2",
    location: {
      city: "New York",
      state: "NY",
      country: "US",
      address: "123 Main St",
      name: "New York Marriott",
      image:
        "https://s7d1.scene7.com/is/image/marriotts7prod/mc-nycmq-lifestyle-37319:Classic-Hor?wid=1720&fit=constrain",
    },
    startDate: "2024-02-01",
    endDate: "2024-02-31",
    price: 2000,
    guests: {
      adults: 2,
      children: 0,
      rooms: 1,
    },
  },
  {
    id: "3",
    location: {
      city: "Rio de Janeiro",
      state: "RJ",
      country: "BR",
      address: "123 Main St",
      name: "RJ Style Hotel",
      image:
        "https://s2-oglobo.glbimg.com/ymhQ8Hs7zuHMLdscKLh4O-IZpwM=/0x0:4256x2832/924x0/smart/filters:strip_icc()/i.s3.glbimg.com/v1/AUTH_da025474c0c44edd99332dddb09cabe8/internal_photos/bs/2022/h/l/egQNisRvKkZ54UBqjElg/3.glbimg.com-v1-auth-0ae9f161c1ff459593599b7ffa1a1292-images-escenic-2019-2f11-2f8-2f17-2f1573246710627.jpg",
    },
    startDate: "2024-03-01",
    endDate: "2024-03-31",
    price: 800,
    guests: {
      adults: 2,
      children: 0,
      rooms: 1,
    },
  },
  {
    id: "4",
    location: {
      city: "Berlin",
      state: "Berlin",
      country: "DE",
      address: "123 Main St",
      name: "Berlin Grand Mercury",
      image:
        "https://cf.bstatic.com/xdata/images/hotel/max1024x768/449886876.jpg?k=fb882fbee7694df9a3b6a232d031dee33462a4ea996ac57c7db5741fca05d868&o=&hp=1",
    },
    startDate: "2024-04-01",
    endDate: "2024-04-31",
    price: 900,
    guests: {
      adults: 2,
      children: 0,
      rooms: 1,
    },
  },
];
