type GuestProps = {
  adults: number;
  children?: number;
  rooms: number;
};

export type LocationProps = {
  name: string;
  image: string;
  address: string;
  city: string;
  country: string;
  state: string;
};

export type BookingProps = {
  id: string;
  location: LocationProps;
  startDate: string;
  endDate: string;
  guests: GuestProps;
  price: number;
};
