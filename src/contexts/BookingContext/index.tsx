import { createContext, useState, ReactNode } from "react";
import { BookingProps } from "./types";
import { AlertModal } from "../../components/Modals/AlertModal";

type BookingContextType = {
  bookings: BookingProps[];
  createBooking: (booking: BookingProps) => void;
  readBooking: (id: string) => BookingProps | undefined;
  updateBooking: (id: string, updatedDetails: BookingProps) => void;
  deleteBooking: (id: string) => void;
};

export const BookingContext = createContext<BookingContextType>(
  {} as BookingContextType
);

type Props = {
  children: ReactNode;
};

export const BookingProvider = ({ children }: Props) => {
  const [bookings, setBookings] = useState<BookingProps[]>([]);

  const createBooking = (booking: BookingProps) => {
    const existingBooking = bookings.find((b) => b.id === booking.id);

    if (!existingBooking) {
      setBookings([...bookings, booking]);
    } else {
      return AlertModal({
        title: "Error",
        type: "error",
        description:
          "You can't add the same booking twice, try with a different hotel",
      });
    }
  };

  const readBooking = (id: string) => {
    return bookings.find((booking) => booking.id === id);
  };

  const updateBooking = (id: string, updatedDetails: BookingProps) => {
    setBookings(
      bookings.map((booking) => (booking.id === id ? updatedDetails : booking))
    );
  };

  const deleteBooking = (id: string) => {
    setBookings(bookings.filter((booking) => booking.id !== id));
  };

  return (
    <BookingContext.Provider
      value={{
        bookings,
        createBooking,
        readBooking,
        updateBooking,
        deleteBooking,
      }}
    >
      {children}
    </BookingContext.Provider>
  );
};
