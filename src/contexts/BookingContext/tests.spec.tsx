import { useContext } from "react";
import { render, act, screen } from "@testing-library/react";
import { BookingContext, BookingProvider } from "./index";
import { BookingProps } from "./types";
import { MOCK_BOOKING } from "./constants";

describe("BookingProvider", () => {
  type TestComponentProps = {
    bookingId: string;
  };

  const TestComponent = ({ bookingId }: TestComponentProps) => {
    const {
      createBooking,
      readBooking,
      updateBooking,
      deleteBooking,
      bookings,
    } = useContext(BookingContext);

    const testBooking: BookingProps = MOCK_BOOKING;

    return (
      <div>
        <button
          data-testid="createButton"
          onClick={() => createBooking(testBooking)}
        >
          Create Booking
        </button>
        <button
          data-testid="updateButton"
          onClick={() =>
            updateBooking(bookingId, {
              ...testBooking /* updated properties */,
            })
          }
        >
          Update Booking
        </button>
        <button
          data-testid="deleteButton"
          onClick={() => deleteBooking(bookingId)}
        >
          Delete Booking
        </button>
        <div data-testid="booking">{readBooking(bookingId)?.id}</div>
        <div data-testid="bookingsCount">{bookings.length}</div>
      </div>
    );
  };

  it("should handles creating, reading, updating, and deleting bookings", async () => {
    const component = render(
      <BookingProvider>
        <TestComponent bookingId={"1"} />
      </BookingProvider>
    );

    expect(component).toMatchSnapshot();

    const createButton = await screen.findByTestId("createButton");
    const updateButton = await screen.findByTestId("updateButton");
    const deleteButton = await screen.findByTestId("deleteButton");
    const bookingDisplay = await screen.findByTestId("booking");
    const bookingsCountDisplay = await screen.findByTestId("bookingsCount");

    act(() => {
      createButton.click();
    });
    expect(bookingDisplay.textContent).toBe("1");
    expect(bookingsCountDisplay.textContent).toBe("1");

    act(() => {
      updateButton.click();
    });

    act(() => {
      deleteButton.click();
    });
    expect(bookingsCountDisplay.textContent).toBe("0");
  });
});
