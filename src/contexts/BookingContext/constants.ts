export const MOCK_BOOKING = {
  id: "1",
  location: {
    city: "New York",
    state: "NY",
    country: "US",
    address: "123 Main St",
    name: "Hotel 1",
    image:
      "https://images.pexels.com/photos/164595/pexels-photo-164595.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  startDate: "2024-01-01",
  endDate: "2024-01-31",
  price: 1000,
  guests: {
    adults: 2,
    children: 0,
    rooms: 1,
  },
};
