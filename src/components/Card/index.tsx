import { BookingProps } from "../../contexts/BookingContext/types";
import { formatAddress } from "../../helpers/formatAddress";
import { formatCurrency } from "../../helpers/formatCurrency";
import { formatDate } from "../../helpers/formatDate";
import { Button } from "../Button";

import "./styles.scss";

type CardProps = BookingProps & {
  onClick?: () => void;
  onDelete?: () => void;
  onEdit?: () => void;
  children?: React.ReactNode;
};

export const Card = ({
  startDate,
  endDate,
  location,
  price,
  guests,
  onClick,
  onDelete,
  children: buttonText,
}: CardProps) => {
  const { name, image } = location;
  const { adults, children, rooms } = guests;

  return (
    <div className="card">
      {onDelete && (
        <button className="card__delete" onClick={onDelete}>
          X
        </button>
      )}
      <img className="card__image" src={image} />
      <div className="card__data">
        <div className="card__data__info">
          <span>{name}</span>
          <strong>{formatCurrency(price)}</strong>
        </div>
        <span className="card__data__location">{`Address : ${formatAddress(
          location
        )}`}</span>
        <div className="card__data__date">
          <span>{`Start Date : ${formatDate(startDate)}`}</span>
          <span>{`End Date : ${formatDate(endDate)}`}</span>
        </div>
        <div className="card__data__guests">
          <span>{adults} Adults</span>
          <span>{children} Children</span>
          <span>{rooms} Rooms</span>
        </div>
      </div>
      <div className="card__data__btn">
        {onClick && <Button {...{ onClick }}>{buttonText}</Button>}
      </div>
    </div>
  );
};
