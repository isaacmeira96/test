import { ModalProps } from "./types";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

export const AlertModal = ({ title, description, type }: ModalProps) => {
  const MySwal = withReactContent(Swal);

  return MySwal.fire({
    icon: type,
    title: title,
    text: description,
  });
};
