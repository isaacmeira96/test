export type ModalProps = {
  title?: string;
  description?: string;
  buttonText?: string;
  buttonAction?: () => void;
  deleteAction?: () => void;
  deletedText?: string;
  type?: "success" | "error" | "warning" | "info";
};
