import { render, screen } from "@testing-library/react";
import { EmptyState } from "./index"; // Adjust the import path

describe("EmptyState", () => {
  const defaultProps = {
    title: "No Data Available",
    description: "No additional details are available at this time.",
  };

  it("renders correctly with an icon", () => {
    const props = {
      ...defaultProps,
      icon: "test-icon.jpg",
      alt: "Test Icon",
    };
    const component = render(<EmptyState {...props} />);

    expect(component).toMatchSnapshot();

    expect(screen.getByAltText("Test Icon")).toBeDefined();
    expect(screen.getByText(props.title)).toBeDefined();
    expect(screen.getByText(props.description)).toBeDefined();
  });

  it("renders correctly without an icon", () => {
    const component = render(<EmptyState {...defaultProps} />);
    expect(component).toMatchSnapshot();

    expect(screen.queryByAltText("empty state icon")).toBeNull();
    expect(screen.getByText(defaultProps.title)).toBeDefined();
    expect(screen.getByText(defaultProps.description)).toBeDefined();
  });
});
