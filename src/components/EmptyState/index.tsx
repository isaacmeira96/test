import { EmptyStateProps } from "./types";
import "./styles.scss";

export const EmptyState = ({
  title,
  description,
  icon,
  alt,
}: EmptyStateProps) => {
  return (
    <div className="empty-state">
      <>
        {icon && <img src={icon} alt={alt ?? "empty state icon"} />}
        <span className="empty-state__title">{title}</span>
        <span className="empty-state__subtitle">{description}</span>
      </>
    </div>
  );
};
