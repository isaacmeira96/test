export type EmptyStateProps = {
  title: string;
  description?: string;
  icon?: string;
  alt?: string;
  children?: React.ReactNode;
};
