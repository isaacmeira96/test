import { ButtonProps } from "./types";
import "./styles.scss";

export const Button = ({ onClick, children, disabled }: ButtonProps) => {
  return (
    <button className="button" {...{ disabled, onClick }}>
      {children}
    </button>
  );
};
