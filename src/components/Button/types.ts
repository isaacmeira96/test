export type ButtonProps = {
  onClick: () => void;
  children: React.ReactNode;
  disabled?: boolean;
};
