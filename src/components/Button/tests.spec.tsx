import { render, fireEvent, screen } from "@testing-library/react";
import { Button } from "./index";

describe("Button", () => {
  it("renders correctly", () => {
    render(<Button onClick={() => {}}>Test Button</Button>);
    const buttonElement = screen.getByText("Test Button");
    expect(buttonElement).toBeDefined();
    expect(buttonElement).toMatchSnapshot();
  });

  it("calls onClick when clicked", () => {
    const mockOnClick = jest.fn();
    render(<Button onClick={mockOnClick}>Clickable</Button>);

    const buttonElement = screen.getByText("Clickable");
    fireEvent.click(buttonElement);
    expect(mockOnClick).toHaveBeenCalledTimes(1);
  });

  it("does not call onClick when disabled", () => {
    const mockOnClick = jest.fn();
    render(
      <Button onClick={mockOnClick} disabled>
        Disabled
      </Button>
    );

    const buttonElement = screen.getByText("Disabled");
    fireEvent.click(buttonElement);
    expect(mockOnClick).not.toHaveBeenCalled();
  });

  it("displays children correctly", () => {
    const buttonText = "Child Text";
    render(<Button onClick={() => {}}>{buttonText}</Button>);

    const buttonElement = screen.getByText(buttonText);
    expect(buttonElement).toBeDefined();
  });
});
