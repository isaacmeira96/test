import { BookingsDomain } from "../../domain/BookingsDomain";
import { SearchDomain } from "../../domain/SearchDomain";
import "./styles.scss";

function Home() {
  return (
    <div className="booking-home">
      <div className="booking-home__banner">
        <h1>Hello Jhon !</h1>
        <span>Discover our amazing places to stay</span>
      </div>
      <div className="booking-home__data">
        <div className="available-places">
          <SearchDomain />
        </div>

        <div className="booking-list">
          <BookingsDomain />
        </div>
      </div>
    </div>
  );
}

export default Home;
